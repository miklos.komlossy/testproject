import paramiko
import getpass
import time

ssh_client = paramiko.SSHClient()

ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

device = {
    "hostname": 'sbx-nxos-mgmt.cisco.com',
    "port": 8181,
    "username": 'admin',
    #"password": getpass.getpass('password> '),
    "password": 'Admin_1234!',
    "look_for_keys": False, 
    "allow_agent": False
}

try:
    ssh_client.connect(**device)
    remote_connection = ssh_client.invoke_shell()
    remote_connection.send('sh ip int brief\n')
    time.sleep(1)
    output = remote_connection.recv(4096)
    print(output.decode())
finally:
    ssh_client.close()